//
//  BluetoothCommands.swift
//  BluetoothTutorial
//
//  Created by Anh Nguyễn Hoàng on 11/28/19.
//  Copyright © 2019 Anh Nguyễn Hoàng. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothService {
    
    func getSettings() {
        self.peripheral?.readValue(for: self.dataCharacteristic!)
    }
    
    // TODO: add other methods to expose high level requests to peripheral
}
