//
//  FlowController.swift
//  BluetoothTutorial
//
//  Created by Anh Nguyễn Hoàng on 11/28/19.
//  Copyright © 2019 Anh Nguyễn Hoàng. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol FlowController {
    func bluetoothOn()
    func bluetoothOff()
    func scanStarted()
    func scanStopped()
    func connected(peripheral: CBPeripheral)
    func disconnected(failure: Bool)
    func discoveredPeripheral()
    func readyToWrite()
    func received(response: Data)
    // TODO: add other events if needed
}

// Default implementation for FlowController
extension FlowController {
    func bluetoothOn() { }
    func bluetoothOff() { }
    func scanStarted() {
        print("")
    }
    func scanStopped() { }
    func connected(peripheral: CBPeripheral) { }
    func disconnected(failure: Bool) { }
    func discoveredPeripheral() { }
    func readyToWrite() { }
    func received(response: Data) { }
}
