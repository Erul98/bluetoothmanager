//
//  BluetoothConnectionHandler.swift
//  BluetoothTutorial
//
//  Created by Anh Nguyễn Hoàng on 11/28/19.
//  Copyright © 2019 Anh Nguyễn Hoàng. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothService: CBCentralManagerDelegate {
    
    var expectedNamePrefix: String { return "GoPro" } // 1. Additional filtering by name, you can skip this step or change logic to your own.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state != .poweredOn {
            print("bluetooth is OFF (\(central.state.rawValue))")
            self.stopScan()
            self.disconnect()
            self.flowController?.bluetoothOff() // 2. Notifying Flow Controller about current state.
        } else {
            print("bluetooth is ON")
            self.flowController?.bluetoothOn() // 2. Notifying Flow Controller about current state.
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard peripheral.name != nil else {
            return
        } // 1.
        print("discovered peripheral: \(peripheral.name!)")
        
        self.peripheral = peripheral
        self.flowController?.discoveredPeripheral()
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if let periperalName = peripheral.name {
            print("connected to: \(periperalName)")
        } else {
            print("connected to peripheral")
        }
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        self.flowController?.connected(peripheral: peripheral) // 2. Notifying Flow Controller about current state.
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("peripheral disconnected")
        self.dataCharacteristic = nil
        self.flowController?.disconnected(failure: false) // 2. Notifying Flow Controller about current state.
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to connect: \(error.debugDescription)")
        self.dataCharacteristic = nil
        self.flowController?.disconnected(failure: true) // 2. Notifying Flow Controller about current state.
    }
}
