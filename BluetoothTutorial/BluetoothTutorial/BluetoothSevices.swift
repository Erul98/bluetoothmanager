//
//  BluetoothSevices.swift
//  BluetoothTutorial
//
//  Created by Anh Nguyễn Hoàng on 11/28/19.
//  Copyright © 2019 Anh Nguyễn Hoàng. All rights reserved.
//

import UIKit
import CoreBluetooth

class BluetoothService: NSObject { // 1. Service must inherit from NSObject in order to be able to assign itself as a delegate for CBCentralManager.
    
    // 2. To make this example easier, I assume that we will communicate only with one service and characteristic, it should be easy to extend it if you need. If you want to see iOS pairing alert, you need to request data from characteristic which is protected – requires encryption. Otherwise, you will just connect to peripheral without bonding.
    let dataServiceUuid = "180A"
    let dataCharacteristicUuid = "2A29"
    
    var centralManager: CBCentralManager!
    var peripheral: CBPeripheral?
    var dataCharacteristic: CBCharacteristic?
    var bluetoothState: CBManagerState {
        return self.centralManager.state
    }
    var flowController: FlowController? // 3. Here we will assign Flow Controller appropriate for current application state.
    
    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func startScan() {
        self.peripheral = nil
        guard self.centralManager.state == .poweredOn else { return }
        
        self.centralManager.scanForPeripherals(withServices: [CBUUID(string: "0x9D6B8AD8-A273-48FE-842B-1FAB20443734")]) // 4. You probably should request here for peripherals only with specific service to filter out devices not related with your application.
        self.flowController?.scanStarted() // 5. Notifying Flow Controller about current state.
        print("scan started")
    }
    
    func stopScan() {
        self.centralManager.stopScan()
        self.flowController?.scanStopped() // 5. Notifying Flow Controller about current state.
        print("scan stopped\n")
    }
    
    func connect() {
        guard self.centralManager.state == .poweredOn else { return }
        guard let peripheral = self.peripheral else { return }
        self.centralManager.connect(peripheral)
    }
    
    func disconnect() {
        guard let peripheral = self.peripheral else { return }
        self.centralManager.cancelPeripheralConnection(peripheral)
    }
}
