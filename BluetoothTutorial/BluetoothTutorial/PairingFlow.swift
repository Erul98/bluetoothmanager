//
//  PairingFlow.swift
//  BluetoothTutorial
//
//  Created by Anh Nguyễn Hoàng on 11/28/19.
//  Copyright © 2019 Anh Nguyễn Hoàng. All rights reserved.
//

import Foundation
import CoreBluetooth

class PairingFlow {
    
    let timeout = 15.0
    var waitForPeripheralHandler: () -> Void = { }
    var pairingHandler: (Bool) -> Void = { _ in }
    var pairingWorkitem: DispatchWorkItem?
    var pairing = false
    
    weak var bluetoothService: BluetoothService? //1. There will be a circular reference between BluetoothService and PairingFlow, therefore this property has to be weak to avoid memory leak.
    
    init(bluetoothService: BluetoothService) {
        self.bluetoothService = bluetoothService
    }
    
    // MARK: Pairing steps
    
    func waitForPeripheral(completion: @escaping () -> Void) {
        self.pairing = false
        self.pairingHandler = { _ in }
        
        self.bluetoothService?.startScan()
        self.waitForPeripheralHandler = completion
    }
    
    func pair(completion: @escaping (Bool) -> Void) {
        guard self.bluetoothService?.centralManager.state == .poweredOn else {
            print("bluetooth is off")
            self.pairingFailed()
            return
        }
        guard let peripheral = self.bluetoothService?.peripheral else {
            print("peripheral not found")
            self.pairingFailed()
            return
        }
        
        self.pairing = true
        self.pairingWorkitem = DispatchWorkItem { // 2. We implement timeout for pairing operation.
            print("pairing timed out")
            self.pairingFailed()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + self.timeout, execute: self.pairingWorkitem!) // 2. We implement timeout for pairing operation.
        
        print("pairing...")
        self.pairingHandler = completion
        self.bluetoothService?.centralManager.connect(peripheral)
    }
    
    func cancel() {
        self.bluetoothService?.stopScan()
        self.bluetoothService?.disconnect()
        self.pairingWorkitem?.cancel()
        
        self.pairing = false
        self.pairingHandler = { _ in }
        self.waitForPeripheralHandler = { }
    }
    
    private func pairingFailed() {
        self.pairingHandler(false)
        self.cancel()
    }
}

// MARK: 3. State handling
extension PairingFlow: FlowController {
    func discoveredPeripheral() {
        self.bluetoothService?.stopScan()
        self.waitForPeripheralHandler()
    }
    
    func readyToWrite() {
        guard self.pairing else { return }
        
        self.bluetoothService?.getSettings() // 4. When Bluetooth is in expected state, we can proceed with sending requests.
    }
    
    func received(response: Data) {
        print("received data: \(String(bytes: response, encoding: String.Encoding.ascii) ?? "")")
        // TODO: validate response to confirm that pairing is sucessful
        self.pairingHandler(true)
        self.cancel()
    }
    
    func disconnected(failure: Bool) {
        self.pairingFailed()
    }
    
    //5. Pairing on iOS is tricky, because system doesn’t inform an application in any way whether peripheral has been paired or not. The only way to check it is to verify if we can read from protected characteristic. You can read more in Swift – Bluetooth Low Energy – how to get paired devices?
}
